// import Uniswap SDK
const { ChainId, Fetcher, WETH, Route, Trade, TokenAmount, TradeType, Percent } = require('@uniswap/sdk');

// import dotenv
require('dotenv').config()

// import web3
var Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider(process.env.INFURA_URL+process.env.INFURA_PROJECT_ID));

// get chainID
var chainId = ChainId.RINKEBY;

// get Token Address
var tokenAddress = process.env.DAI_TOKEN_ADDRESS;

// get ABI
var getAbi = require('./abi');
var uniswapRouterABI = getAbi.abi();

(async() => {

   // dai Object
   let daiObj = await Fetcher.fetchTokenData(chainId, tokenAddress);

   // weth Object
   let wethObj = WETH[chainId];

   // getPair
   let getPair = await Fetcher.fetchPairData(daiObj, wethObj);

   // get route
   let route = new Route([getPair], wethObj);

   // get Current Trade Details -> 0.5 ETH
   let trade = new Trade(route, new TokenAmount(wethObj, '500000000000000000'), TradeType.EXACT_INPUT);

   // set Tolerance to 1%
   let tolerance = new Percent('1', '100');
   let amountOutMin = trade.minimumAmountOut(tolerance).raw;

   // path
   let path = [wethObj.address, daiObj.address];

   // Set 10 min deadline
   let deadline = Math.floor(Date.now() / 1000) + 60 * 10;

   // Value to Trade
   let valueToTrade = trade.inputAmount.raw;

   // Get Contract details from .env - Created for testing purpose, won't use it anymore
   let uniswapRouterContract = process.env.UNISWAP_ROUTE_CONTRACT;
   let privateKey = process.env.TEST_USER_PRIVATE_KEY;
   let publicKey = process.env.TEST_USER_PUBLIC_ADDRESS;

   // Uniswap Route2 Object
   let Uniswap = await new web3.eth.Contract(uniswapRouterABI, uniswapRouterContract);

   // Prepare tx object
   const tx = {
      from: publicKey,
      to: uniswapRouterContract,
      gas: 6000000,
      value: valueToTrade.toString(),
      gasPrice: 30e9,
      data: Uniswap.methods.swapETHForExactTokens(amountOutMin.toString(), path, publicKey, deadline).encodeABI()
   };

   // Sign Promise
   const signPromise = web3.eth.accounts.signTransaction(tx, privateKey);

   // Submit Transaction
   signPromise.then((signedTx) => {
      const sentTx = web3.eth.sendSignedTransaction(signedTx.raw || signedTx.rawTransaction);
      console.log("Transaction initiated....")
      console.log("Waiting for confirmation....")
      sentTx.on("receipt", receipt => {
         console.log(receipt);
      });
      sentTx.on("error", err => {
         console.log(err);
      });
   }).catch((err) => {
      console.log(err);
   });

})()